//
//  UIAccessibility+Testing.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 27/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation
import UIKit


/**
 Accessibility Protocol to enable ui test in ui elements inside view controllers
 */
protocol UIAccessibilityTesting: class {
    func prepareAccessibilityTesting()
}

/**
 Accessibility Protocol to define new accessibility labels
 */
protocol BaseAccessibilityTesting {
    var rawValue:String { get }
}

/**
 Define accessibility for UI Testing
 */
extension NSObject {
    
    var accessibilityTesting:BaseAccessibilityTesting? {
        get { return nil }
        
        set(accessibilityKey) {
            #if APPSTORE
                return
            #endif
            
            self.accessibilityLabel = accessibilityKey?.rawValue
        }
    }
}


/**
 Define ui test in cells: common iterable enum + helper
 */
enum IterableAccessibilityTesting: BaseAccessibilityTesting {
    
    case common(indexPath:IndexPath, parent:String?, element:IterableElement)
    case iterable(parent:String?, cell:String?, element:IterableElement)
    
    
    var rawValue:String {
        
        switch self {
        case let .common(indexPath, parent, element):
            return accessibilityTestingForCommonIterable(at:indexPath, inParent:parent, element:element)
            
        case let .iterable(parent, cell, element):
            return accessibilityTestingForIterable(inParent:parent, with:cell, element:element)
        }
    }
    
    
    private func accessibilityTestingForCommonIterable(at indexPath:IndexPath, inParent parent:String?, element:IterableElement) -> String {
        guard let parent = parent, parent.characters.count > 0 else {
            fatalError("Common iterable element should have a defined accessible testing ViewGroup (TableView|CollectionView)")
        }
        
        if case let .item(elementAccessibility) = element {
            return "\(indexPath.section)_\(indexPath.row)__\(parent)+\(elementAccessibility)"
        }
        return "\(indexPath.section)_\(indexPath.row)__\(parent)"
    }
    
    private func accessibilityTestingForIterable(inParent parent:String?, with accessibility:String?, element:IterableElement) -> String {
        guard let accessibility = accessibility, accessibility.characters.count > 0,
              let parent = parent, parent.characters.count > 0 else {
            fatalError("Iterable Element should have a defined accessible testing ViewGroup (TableView|CollectionView)")
        }
        
        if case let .item(elementAccessibility) = element {
            return "\(parent)__\(accessibility)+\(elementAccessibility)"
        }
        return "\(parent)__\(accessibility)"
    }
}

enum IterableElement {
    case cell
    case item(accessibility: String)
}


extension NSObject {
    
    func prepareIterableAccessibilityTesting(indexPath: IndexPath, viewGroup accessibilityParent: String) {
        clearAccessibility()
        guard let cell = self as? UIAccessibilityTesting else {
            prepareIterableAccessibilityTesting(at: indexPath, accessibilityCell: nil, accessibilityParent: accessibilityParent)
            return
        }
        
        cell.prepareAccessibilityTesting()
        
        if let elements = self.accessibilityElements {
            prepareIterableAccessibilityTesting(at: indexPath, cell: cell, accessibilityCell: self.accessibilityLabel, accessibilityParent: accessibilityParent, accessibilityElements:elements)
        } else {
            prepareIterableAccessibilityTesting(at: indexPath, accessibilityCell: self.accessibilityLabel, accessibilityParent: accessibilityParent)
        }
    }

    
    func clearAccessibility() {
        self.accessibilityLabel = nil
        self.accessibilityElements?.forEach { ($0 as? UIView)?.accessibilityLabel = nil }
    }
    
    func disableAccessibility() {
        self.isAccessibilityElement = false
        self.accessibilityElements?.forEach { ($0 as? UIView)?.isAccessibilityElement = false }
    }
    
    private func prepareIterableAccessibilityTesting(at indexPath: IndexPath, cell: UIAccessibilityTesting, accessibilityCell: String?, accessibilityParent: String, accessibilityElements: [Any]) {
    
        self.isAccessibilityElement = false
        
        accessibilityElements.forEach {
            guard let element = $0 as? UIView, let accessibilityLabel = element.accessibilityLabel else { return }
            
            if let accessibilityCell = accessibilityCell {
                element.accessibilityTesting = IterableAccessibilityTesting.iterable(parent: accessibilityParent,
                                                                                     cell: accessibilityCell,
                                                                                     element: .item(accessibility: accessibilityLabel))
            } else {
                element.accessibilityTesting = IterableAccessibilityTesting.common(indexPath: indexPath,
                                                                                   parent: accessibilityParent,
                                                                                   element: .item(accessibility: accessibilityLabel))
            }
        }
    }
    
    private func prepareIterableAccessibilityTesting(at indexPath: IndexPath, accessibilityCell: String?, accessibilityParent: String) {
        
        if let accessibilityCell = accessibilityCell {
            self.accessibilityTesting = IterableAccessibilityTesting.iterable(parent: accessibilityParent, cell: accessibilityCell, element: .cell)
        } else {
            self.accessibilityTesting = IterableAccessibilityTesting.common(indexPath: indexPath, parent: accessibilityParent, element: .cell)
        }
    }
    
}

