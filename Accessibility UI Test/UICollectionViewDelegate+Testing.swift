//
//  UICollectionViewDelegate+Testing.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 30/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation
import UIKit


extension NSObject {
    
    func custom_collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.responds(to: #selector(self.custom_collectionView(_:willDisplay:forItemAt:))) {
            self.custom_collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        }
        guard let accessibilityViewGroup = collectionView.accessibilityLabel, accessibilityViewGroup.characters.count > 0 else { return }
        
        cell.isAccessibilityElement = true
        cell.prepareIterableAccessibilityTesting(indexPath: indexPath, viewGroup: accessibilityViewGroup)
    }
}


@objc protocol UICollectionViewAccessibilityTesting { }
extension UICollectionViewAccessibilityTesting where Self: UICollectionViewDelegate {
    
    static func registerAccessibilityCellUITest() {
        #if APPSTORE
            return
        #endif
        
        DispatchQueue.once(token: String(describing: self) + ".CollectionViewDelegate.accessibilityTesting.swizzle") {
            UIAccessibilityUtil.swizzled(method: #selector(self.collectionView(_:willDisplay:forItemAt:)),
                                         with:   #selector(NSObject.custom_collectionView(_:willDisplay:forItemAt:)),
                                         inClass: self, usingClass: NSObject.self)
        }
    }
}

