//
//  UIViewController+Testing.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 28/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    @objc private func custom_viewWillAppear(_ animated: Bool) {
        guard let accessibilityController = self as? UIAccessibilityTesting else { return }
        
        self.isAccessibilityElement = true
        accessibilityController.prepareAccessibilityTesting()
    }
    
    static func registerAccessibilityUITest() {
        #if APPSTORE
            return
        #endif
        
        DispatchQueue.once(token: "UIViewController.accessibilityTesting.swizzle") {
            UIAccessibilityUtil.swizzled(method: #selector(UIViewController.viewWillAppear(_:)),
                                         with:   #selector(UIViewController.custom_viewWillAppear(_:)),
                                         inClass: UIViewController.self, usingClass: UIViewController.self)
        }
    }
}

