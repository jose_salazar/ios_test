//
//  UITestConfiguration.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 28/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation
import UIKit


extension AppDelegate {
    
    func initializeUITest() {
        #if APPSTORE
            return
        #endif
        
        MainTableViewController.registerAccessibilityCellUITest()
        SecoundaryTableViewController.registerAccessibilityCellUITest()
        UIViewController.registerAccessibilityUITest()
    }
}

extension MainTableViewController: UITableViewAccessibilityTesting { }
extension SecoundaryTableViewController: UITableViewAccessibilityTesting { }
