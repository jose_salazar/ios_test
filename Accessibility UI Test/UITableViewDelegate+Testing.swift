//
//  UITableViewDelegate+Testing.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 28/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation
import UIKit


extension NSObject {
    
    public func custom_tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.responds(to: #selector(self.custom_tableView(_:willDisplay:forRowAt:))) {
            self.custom_tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        }
        
        guard let accessibilityViewGroup = tableView.accessibilityLabel, accessibilityViewGroup.characters.count > 0 else { return }
        
        cell.prepareIterableAccessibilityTesting(indexPath: indexPath, viewGroup: accessibilityViewGroup)
    }
}


@objc protocol UITableViewAccessibilityTesting { }
extension UITableViewAccessibilityTesting where Self: UITableViewDelegate {
    
    static func registerAccessibilityCellUITest() {
        #if APPSTORE
            return
        #endif
        
        DispatchQueue.once(token: String(describing: self) + ".TableViewDelegate.accessibilityTesting.swizzle") {
            UIAccessibilityUtil.swizzled(method: #selector(self.tableView(_:willDisplay:forRowAt:)),
                                         with:   #selector(NSObject.custom_tableView(_:willDisplay:forRowAt:)),
                                         inClass: self, usingClass: NSObject.self)
        }
    }
}

