//
//  UIAccessibilityUtil.swift
//  ios-whitelabel
//
//  Created by Miguel Angel Diaz Lopez on 28/6/17.
//  Copyright © 2017 ODIGEO. All rights reserved.
//

import Foundation


struct UIAccessibilityUtil {
    
    static func swizzled(method originalSelector:Selector, with swizzledSelector:Selector, inClass:AnyClass, usingClass:AnyClass) {
        let originalMethod = class_getInstanceMethod(inClass, originalSelector)
        let swizzledMethod = class_getInstanceMethod(usingClass, swizzledSelector)
        
        if (class_addMethod(inClass, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))) {
            class_replaceMethod(inClass, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
        
    }
}

