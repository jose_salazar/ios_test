//
//  BasicViewController+UITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 27/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation


extension BasicViewController: UIAccessibilityTesting {
    
    func prepareAccessibilityTesting() {
        self.button.accessibilityTesting = BasiModuleUITest.button
        self.anotherButton.accessibilityTesting = BasiModuleUITest.anotherButton
        self.yetAnotherButton.accessibilityTesting = BasiModuleUITest.yetAnotherButton
    }
    
}
