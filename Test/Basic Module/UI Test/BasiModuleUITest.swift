//
//  BasiModuleUITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 27/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation


enum BasiModuleUITest: String, BaseAccessibilityTesting {
    
    case button           = "BasicViewController.UIButton.First"
    case anotherButton    = "BasicViewController.UIButton.Second"
    case yetAnotherButton = "BasicViewController.UIButton.Third"
}

