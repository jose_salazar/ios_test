//
//  BasicViewController.swift
//  Test
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import UIKit

class BasicViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var anotherButton: UIButton!
    @IBOutlet weak var yetAnotherButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
