//
//  ExerciseCell+UITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 31/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation


extension ExerciseCell {
    override open var accessibilityElements: [Any]? {
        set(elements) { super.accessibilityElements = elements }
        get {
            return [button1, button2]
        }
    }
}


extension ExerciseCell: UIAccessibilityTesting {
    
    func prepareAccessibilityTesting() {
        self.accessibilityTesting = ExerciseModuleUITest.cell
        self.button1.accessibilityTesting = ExerciseModuleUITest.button1
        self.button2.accessibilityTesting = ExerciseModuleUITest.button2
    }
}

