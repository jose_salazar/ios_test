//
//  SecoundaryTableViewController+UITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 31/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation

extension SecoundaryTableViewController: UIAccessibilityTesting {
    
    func prepareAccessibilityTesting() {
        self.table.accessibilityTesting = ExerciseModuleUITest.table
    }
}
