//
//  ExerciseModuleUITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 31/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation

enum ExerciseModuleUITest: String, BaseAccessibilityTesting {
    
    case table = "SecoundaryTableViewController.UITableView"
    
    case cell  = "UITableViewCell.Secondary"
    case button1
    case button2
}


extension ExerciseModuleUITest {
    
    static var accessibleButton1: IterableAccessibilityTesting {
        return .iterable(parent: ExerciseModuleUITest.table.rawValue,
                         cell: ExerciseModuleUITest.cell.rawValue,
                         element: .item(accessibility: ExerciseModuleUITest.button1.rawValue))
    }
    
    static var accessibleButton2: IterableAccessibilityTesting {
        return .iterable(parent: ExerciseModuleUITest.table.rawValue,
                         cell: ExerciseModuleUITest.cell.rawValue,
                         element: .item(accessibility: ExerciseModuleUITest.button2.rawValue))
    }
}
