//
//  MainTableViewController.swift
//  Test
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ProcessInfo.processInfo.environment["mockServer"] ?? "Test"
    }
    
}
