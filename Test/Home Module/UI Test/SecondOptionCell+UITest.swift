//
//  OptionCell+UITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 27/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation


extension SecondOptionCell: UIAccessibilityTesting {
    
    func prepareAccessibilityTesting() {
        self.accessibilityTesting = HomeModuleUITest.secondOptionCell
    }
}

