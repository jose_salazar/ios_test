//
//  MainTableViewController+UITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 27/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation

extension MainTableViewController: UIAccessibilityTesting {
    
    func prepareAccessibilityTesting() {
        self.tableView?.accessibilityTesting = HomeModuleUITest.table
    }
}
