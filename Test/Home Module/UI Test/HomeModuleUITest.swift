//
//  HomeModuleUITest.swift
//  Test
//
//  Created by Miguel Angel Diaz Lopez on 27/7/17.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import Foundation

enum HomeModuleUITest: String, BaseAccessibilityTesting {
    
    case table = "MainTableViewController.UITableView"
    case optionCell = "UITableViewCell.Option"
    case secondOptionCell = "UITableViewCell.SecondOption"
}

extension HomeModuleUITest {
    
    static var accessibilityOptionCell:IterableAccessibilityTesting {
        return IterableAccessibilityTesting.iterable(parent: HomeModuleUITest.table.rawValue,
                                                     cell:   HomeModuleUITest.optionCell.rawValue,
                                                     element: .cell)
    }
    
    static var accessibilitySecondOptionCell:IterableAccessibilityTesting {
        return IterableAccessibilityTesting.iterable(parent: HomeModuleUITest.table.rawValue,
                                                     cell:   HomeModuleUITest.secondOptionCell.rawValue,
                                                     element: .cell)
    }
}
