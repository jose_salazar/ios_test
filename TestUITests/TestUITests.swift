//
//  TestUITests.swift
//  TestUITests
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//

import XCTest

class TestUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        app = XCUIApplication()
        
        if let value = ProcessInfo.processInfo.environment["mockServer"] {
            app.launchEnvironment["mockServer"] = value
        }
        
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample1() {
        let mainPage = MainTablePage(app: app)
                let basicPage = mainPage.goToBasicPage()
        _ = basicPage.goToSecoundaryTablePage()
    }
    
    func testExample2() {
        let mainPage = MainTablePage(app: app)
        _ = mainPage.goToBasicPage()
    }
    
    func testExample3() {
        let mainPage = MainTablePage(app: app)
        let basicPage = mainPage.goToBasicPage()
        let secondaryPage = basicPage.goToSecoundaryTablePage()
        _ = secondaryPage.goToBasicPage()
    }
}

