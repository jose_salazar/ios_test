//
//  SecoundaryTablePage.swift
//  TestUITests
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//
import XCTest


class SecoundaryTablePage: Page {
    
    var firstButton:XCUIElement  { return app.tables[ExerciseModuleUITest.table.rawValue].buttons[ExerciseModuleUITest.accessibleButton1.rawValue].firstMatch }
    var secondButton:XCUIElement { return app.tables[ExerciseModuleUITest.table.rawValue].buttons[ExerciseModuleUITest.accessibleButton2.rawValue].firstMatch }
    
    func goToBasicPage() -> AnotherBasicPage {
        firstButton.tap()
        return AnotherBasicPage(app: app)
    }
}
