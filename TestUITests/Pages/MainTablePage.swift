//
//  MainTablePage.swift
//  TestUITests
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//
import XCTest

class MainTablePage: Page {
    var firstCell: XCUIElement {
        return app.cells[HomeModuleUITest.accessibilityOptionCell.rawValue].firstMatch
    }
    var secondCell: XCUIElement {
        return app.cells[HomeModuleUITest.accessibilitySecondOptionCell.rawValue].firstMatch
    }
    
    func goToBasicPage() -> BasicPage {
        firstCell.tap()
        return BasicPage(app: app)
    }
}
