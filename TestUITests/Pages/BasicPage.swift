//
//  BasicPage.swift
//  TestUITests
//
//  Created by Jose Salazar on 27/07/2017.
//  Copyright © 2017 Jose Salazar. All rights reserved.
//
import XCTest

class BasicPage: Page {
    var button: XCUIElement {
        return app.buttons[BasiModuleUITest.button.rawValue].firstMatch
    }
    var anotherButton: XCUIElement {
        return app.buttons[BasiModuleUITest.anotherButton.rawValue].firstMatch
    }
    var yetAnotherButton: XCUIElement {
        return app.buttons[BasiModuleUITest.yetAnotherButton.rawValue].firstMatch
    }
    
    func goToSecoundaryTablePage() -> SecoundaryTablePage {
        button.tap()
        return SecoundaryTablePage(app: app)
    }
}
